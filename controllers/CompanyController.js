// CompanyController.js

const Company = require('../models/Company');
const Contact = require('../models/Contact');
const sort = require('./sort_items.js');

const company_not_found =
    {message: 'The company you are looking for does not exist.'};
const contact_not_found =
    {message: 'The contact you are looking for does not exist.'};


// Creates a new company
exports.create = function(req, res) {
  Company.find({title: req.body.title}, {_id: 1}, function(err, company) {
    if (company.length)
      res.redirect(307, 'company/' + company[0]._id);
    else {
      const new_company = new Company(req.body);

      add_to_contacts(req.body.contacts, new_company);

      new_company.save()
      .then(new_company => {
        res.status(201).json(new_company);
      })
      .catch(err => {
        console.log(err);
        res.status(400).send("Unable to save to database.");
      });
    }
  });
}


// Returns a single company
exports.get_company = function(req, res, next) {
  Company.findById(req.params.id).populate('contacts._id').exec(
    function(err, company) {
    if (!company)
      res.status(400).json(company_not_found);
    else {
      res.json(company);
    }
  });
}


// Updates an existing company
exports.set_company = function(req, res, next) {
  Company.findById(req.params.id, function(err, orig_company) {
    if (!orig_company)
      res.status(400).json(company_not_found);
    else {
            // The Company model stores contact id's and not actual contacts
            const upd_company = new Company(req.body);

            // calculate the difference between original and updated company
            const diff = sort.compare_items(orig_company.contacts, upd_company.contacts);

            if (diff.add_item.length)
              add_to_contacts(diff.add_item, upd_company);

            if (diff.subtract_item.length)
              remove_from_contacts(diff.subtract_item, orig_company);

            // Don't resave a company with no changes.
            if ((diff.add_item.length + diff.subtract_item.length == 0)
              && (orig_company.title == upd_company.title)
              && (orig_company.website == upd_company.website))
              res.json(orig_company);
            else {
            // Save the a company with changes
            orig_company.title = upd_company.title;
            orig_company.website = upd_company.website;
            orig_company.contacts = upd_company.contacts;
            orig_company.save().then(orig_company => {
              res.json(orig_company);
              console.log('Saving company.');
            })
            .catch(err => {
              res.status(400).send('Unable to update the database.');
            });
          }

        }
      });
}


// Returns a company's contact list
exports.get_contacts = function(req, res, next) {
  Company.findById(req.params.id, function(err, company) {
    if (!company)
      res.status(400).json(contact_not_found);
    else {
      Contact.find({_id: {"$in": company.contacts}}, function(err, result) {
        if (err)
          console.log(err);
        else
          res.status(200).json(result);
      });
    }
  });
}


// Returns all companies
exports.get_companies = function(req, res) {
  Company.find().populate('contacts._id').exec(function(err, companies) {
    if (err) {
      console.log(err);
    }
    else {
      res.json(companies);
    }
  });
}


// Add the company id to the newly associated contacts
add_to_contacts = function(member, company) {
  Contact.updateMany({_id: {$in: member}}, {$addToSet: {companies: company}},
    function(err, result) {
    if (err)
      console.log(err);
    else
      console.log('Company was added to its associated contact(s).');
  });
}


// Remove the company id from the newly disassocited contacts.
// Contacts are deleted if they have no relationship with any company.
remove_from_contacts = function(member, company) {
  Contact.updateMany({_id: {$in: member}}, {$pull: {companies: company}},
    function(err, result) {
    if (err)
      console.log(err);
    else {
      Contact.deleteMany({"companies.0": {"$exists": false}},
        function(err, results) {
        if (err)
          console.log(err);
        else
          console.log('Contact(s) with less than 1 company have been deleted.');
      })
      console.log('Company was removed from its associated contact(s).');
    }
  });
};
