// ContactController.js

const Company = require('../models/Company');
const Contact = require('../models/Contact');
const sort = require('./sort_items.js');

const contact_not_found =
    {message: 'The contact you are looking for does not exist.'};
const company_not_found = 
    {message: 'The company you are looking for does not exist.'};


// Creates a new contact
exports.create = function(req, res) {
  Contact.find({first_name: req.body.first_name, surname: req.body.surname}, 
    {_id: 1}, function(err, contact) {
    if (contact.length) {
      res.redirect(307, 'contact/' + contact[0]._id);
    }
    else {
      const new_contact = new Contact(req.body);
      associated_companies = sort.remove_duplicates(new_contact.companies);
      add_to_companies(associated_companies, new_contact);

      new_contact.save()
      .then(contact => {
        res.status(201).json(new_contact);
      })
      .catch(err => {
        res.status(400).json({message: 'Unable to save to database.'});
      });
    }
  });
}


// Returns a single contact
exports.get_contact = function(req, res, next) {
  Contact.findById(req.params.id, function(err, contact) {
    if (!contact)
      res.status(400.).json(contact_not_found);
    else {
      res.json(contact);
    }
  });
}


// Updates an existing contact
exports.set_contact = function(req, res, next) {
  Contact.findById(req.params.id, function(err, orig_contact) {
    if (!orig_contact)
      res.status(400.).json(contact_not_found);
    else {
      const upd_contact = new Contact(req.body);
      
      const diff = sort.compare_items(orig_contact.companies, upd_contact.companies);

      if (diff.add_item.length)
        add_to_companies(diff.add_item, upd_contact);

      if (diff.subtract_item.length)
        remove_from_companies(diff.subtract_item, orig_contact);

      if ((diff.add_item.length + diff.subtract_item.length == 0)
        && (orig_contact.first_name == upd_contact.first_name)
        && (orig_contact.surname == upd_contact.surname))
        res.json(orig_contact);
      else {
        orig_contact.first_name = upd_contact.first_name;
        orig_contact.surname = upd_contact.surname;
        orig_contact.companies = upd_contact.companies;
        console.log(upd_contact.companies)
        orig_contact.save().then(orig_contact => {
          res.json(orig_contact);
        })
        .catch(err => {
          res.status(400).send('Unable to update the database.');
        });
      }
    }
  });
}


// Returns the contact's companies
exports.get_companies = function(req, res, next) {
  Contact.findById(req.params.id, {
    companies: 1,
    _id: 0
  }).populate('companies._id', {title: 1}).exec(function(err, contact) {
    if (!contact)
      res.status(400.).json(contact_not_found);
    else {
      res.json(contact);
    }
  });
}


// Returns all the contacts
exports.get_contacts = function(req, res) {
  Contact.find(function(err, contacts) {
    if (err) {
      console.log(err);
    }
    else {
      res.json(contacts);
    }
  });
}


// Add the contact id to the newly associated companies.
const add_to_companies = function(member, contact) {
  Company.updateMany({_id: {$in: member}}, {$addToSet: {contacts: contact}},
    function(err, result) {
    if (err)
      console.log(err);
    else {
      console.log('Company(s) updated with contact details.');
    }
  });
};


// Remove the contact id from the newly disassocited companies.
const remove_from_companies = function(member, contact) {
  Company.updateMany({_id: {$in: member}}, {$pull: {contacts: contact}},
    function(err, result) {
    if (err)
      console.log(err);
    else
      console.log('Contact was removed from its associated company(s).');
  });
};
