// Sort_items.js
const mongoose = require('mongoose');

// Removes duplicates from an array of ids
module.exports.remove_duplicates = function(a) {
  // convert mongoose id to string
  array = a.map(x => x._id.toString());

  for (i = 0; i < array.length; i++) {
    if (array.indexOf(array[i]) != i)
      array.splice(i, 1);
  }

  //convert string back to mongoose id
  return array.map(function(x) {
    return {_id: mongoose.Types.ObjectId(x)};
  });
}


// finds the items that array_1 must subtract and add to become array_2
module.exports.compare_items = function(array_1, array_2) {

  var add_item = [];
  var subtract_item = [];
  var unchanged_item = [];

  // Remove duplicates from incoming request and convert object id to string.
  array_1 = array_1.map(a => a._id.toString());
  array_2 = module.exports.remove_duplicates(array_2).map(a => a._id.toString());


  for (i = 0; i < array_2.length; i++) {
    if (array_1.indexOf(array_2[i]) == -1) {
      add_item.push(array_2[i]);
    }
    else
      unchanged_item.push(array_2[i]);
  }

  for (i = 0; i < array_1.length; i++) {
    if (array_2.indexOf(array_1[i]) == -1) {
      subtract_item.push(array_1[i]);
    }
  }


  // convert strings back to mongoose _id
  if (add_item.length)
    add_item : add_item.map(function(x) {
      return {_id: mongoose.Types.ObjectId(x)};
    });

  if (subtract_item.length)
    subtract_item: subtract_item.map(function(x) {
      return {_id: mongoose.Types.ObjectId(x)};
    });

  if (unchanged_item.length) {
    unchanged_item = unchanged_item.map(function(x) {
      return {_id: mongoose.Types.ObjectId(x)};
    });
  }

  return diff = {
    add_item: add_item,
    subtract_item: subtract_item,
    unchanged_item: unchanged_item,
  };
};
