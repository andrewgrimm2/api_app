// index.js
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


const app = express();
const config = require('./db');
const PORT = 7070;

app.use(bodyParser.json());


// Models
const Company = require('./models/Company');
const Contact = require('./models/Contact');


// Routes
const CompanyRoute = require('./routes/CompanyRoute');
const CompaniesRoute = require('./routes/CompaniesRoute');
const ContactsRoute = require('./routes/ContactsRoute');
const ContactRoute = require('./routes/ContactRoute');
app.use('/company', CompanyRoute);
app.use('/companies', CompaniesRoute);
app.use('/contacts', ContactsRoute);
app.use('/contact', ContactRoute);

// 404 page
app.use('*', function (req, res, next) {
  res.status(404).json({"Titile": "Page not found"});
})


module.exports = app;


mongoose.connect(config.DB, {useNewUrlParser: true}).then(
  () => {
    console.log('Database is connected')
  },
  err => {
    console.log('Can not connect to the database' + err)
  });


app.listen(PORT, function () {
  console.log('Your node js server is running on PORT: ', PORT);
});
