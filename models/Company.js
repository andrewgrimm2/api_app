// Company.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema for company
var Company = new Schema({
  title: {
    type: String,
    required: true
  },
  website: {
    type: String
  },
  contacts: [{_id: {type: Schema.ObjectId, ref: 'Contact'}}]
}, {
  versionKey: false,
  collection: 'companies'
});

module.exports = mongoose.model('Company', Company);
