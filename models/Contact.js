// Contact.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema for a contacts relationship with a company
var Profile = new Schema({
  _id: {type: Schema.ObjectId, ref: 'Company', required: true},
  email: {type: String},
  phone: {type: String},
}, {
  versionKey: false,
})


// Schema for contact
var Contact = new Schema({
  first_name: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  companies: {
    type: [Profile],
    validate: [arrayLimit, '{PATH}  Contact requires at least 1 company profile.']
  }
}, {
  versionKey: false,
  collection: 'contacts'
});

function arrayLimit(val) {
  return val.length >= 1;
}

module.exports = mongoose.model('Contact', Contact);
