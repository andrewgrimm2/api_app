// CompaniesRoute.js

const express = require('express');
const router = express.Router();

const company_controller = require('../controllers/CompanyController');

// Get all companies
router.route('')
    .get(company_controller.get_companies);

module.exports = router;
