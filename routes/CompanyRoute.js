// CompanyRoute.js

const express = require('express');
const router = express.Router();

const company_controller = require('../controllers/CompanyController');


// Create new company
router.route('')
    .post(company_controller.create);

// Get and set company details
router.route('/:id')
    .get(company_controller.get_company)
    .post(company_controller.set_company);

// Get company contacts
router.route('/:id/contacts')
    .get(company_controller.get_contacts);

module.exports = router;
