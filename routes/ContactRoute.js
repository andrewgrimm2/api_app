// ContactRoute.js

const express = require('express');
const router = express.Router();

const contact_controller = require('../controllers/ContactController')

// Create new contact
router.route('')
    .post(contact_controller.create);

// Get and set contact details
router.route('/:id')
    .get(contact_controller.get_contact)
    .post(contact_controller.set_contact);

// Get contact company list
router.route('/:id/companies')
    .get(contact_controller.get_companies);

module.exports = router;
