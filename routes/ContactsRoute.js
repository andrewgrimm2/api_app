// ContactsRoute.js

const express = require('express');
const router = express.Router();

const contact_controller = require('../controllers/ContactController');

// Get all contacts
router.route('')
    .get(contact_controller.get_contacts);

module.exports = router;
