// company_test.js

let mongoose = require("mongoose");
const Company = require('../models/Company');
const Contact = require('../models/Contact');


const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);


describe('API endpoint /', function () {
    this.timeout(5000); // How long to wait for a response (ms)

    before((done) => {
        // Clear the companies collection and repopulate companies testing data
        Company.deleteMany({}, (err) => {
            insert_company_test_data(done);
        });

    });


    before((done) => {
        // Clear the contacts collection and repopulate with testing data
        Contact.deleteMany({}, (err) => {
            insert_contact_test_data(done);
        });
    });


    describe('/GET companies', () => {

        // test that all companies were successfully saved in the database
        it('it should GET all the companies', (done) => {
            chai.request(server)
                .get('/companies')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(11);
                    done();
                });
        });
    });

    describe('/GET company/:id', () => {
        it('it should GET one company', (done) => {
            chai.request(server)
                .get('/company/' + company_id_array[0]._id)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.be.json;
                    expect(res.body._id).to.equal(company_id_array[0]._id.toString());
                    done();
                });
        });

    });


    describe('/POST company/', () => {
        it('it should be able to create a new company', function () {
            return chai.request(server)
                .post('/company')
                .send({"title": "New Company", "website": "www.new.com", "contacts": []})
                .then(function (res) {
                    expect(res).to.have.status(201);
                    expect(res).to.be.json;
                    expect(res.body).to.be.an('object');
                    expect(res.body).to.have.property('title');
                    expect(res.body).to.have.property('website');
                    expect(res.body).to.have.property('contacts')
                    expect(res.body.contacts).to.have.lengthOf(0);
                });
        });

        it('it should be able to route to company/:id', function () {
            return chai.request(server)
                .post('/company/' + company_id_array[0]._id)
                .send({"title": "New Company", "website": "www.new.com", "contacts": []})
                .then(function (res) {
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    expect(res.body).to.be.an('object');
                    expect(res.body).to.have.property('title');
                    expect(res.body.title).to.equal("New Company");
                    expect(res.body).to.have.property('website');
                    expect(res.body).to.have.property('contacts')
                    expect(res.body.contacts).to.have.lengthOf(0);
                    expect(res.body._id).to.equal(company_id_array[0]._id.toString());
                });
        });
    });


// This function inserts company data into the database and populates an array with the companies id number.
    const insert_company_test_data = function (done) {
        Company.insertMany(company_array).then((docs) => {
            Company.find({}, {_id: 1}, function (err, result) {
                if (err)
                    console.log(err);
                else {
                    company_id_array = result;
                    done();
                }
            });

        }).catch((err) => {
            console.log(err);
        })
    };


    const insert_contact_test_data = function (done) {
        var contact_array = [
            {
                "first_name": "Duke",
                "surname": "Nukem",
                "companies": [{"_id": company_id_array[0]._id, "email": "duke@apogee.com", "phone": "123123"}]
            },
            {
                "first_name": "Lara",
                "surname": "Croft",
                "companies": [{
                    "_id": company_id_array[2]._id,
                    "email": "lara@corestudios.com",
                    "phone": "123123"
                }, {"_id": company_id_array[1]._id, "email": "lara@ediosinteractive.com", "phone": "123123"}]
            },
            {
                "first_name": "Gordon",
                "surname": "Freeman",
                "companies": [{
                    "_id": company_id_array[3]._id,
                    "email": "gordon@valve.com",
                    "phone": "123123"
                }, {"_id": company_id_array[4]._id, "email": "gordon@sierrastudios.com", "phone": "123123"}]
            },
            {
                "first_name": "Guybrush",
                "surname": "Threepwood",
                "companies": [{
                    "_id": company_id_array[5]._id,
                    "email": "guybrush@lucasfilmgames.com",
                    "phone": "123123"
                }]
            },
            {
                "first_name": "Cloud",
                "surname": "Strife",
                "companies": [{
                    "_id": company_id_array[6]._id,
                    "email": "cloud@squareenix.com",
                    "phone": "123123"
                }, {
                    "_id": company_id_array[7]._id,
                    "email": "cloud@ediosinteractive.com",
                    "phone": "123132"
                }, {"_id": company_id_array[8]._id, "email": "cloud@sony.com", "phone": "123123"}]
            },
            {
                "first_name": "Solid",
                "surname": "Snake",
                "companies": [{"_id": company_id_array[9]._id, "email": "solid@konami.com", "phone": "123123"}]
            },
            {
                "first_name": "Nathan",
                "surname": "Drake",
                "companies": [{"_id": company_id_array[10]._id, "email": "nathan@naughtydog.com", "phone": "123123"}]
            }
        ]

        for (i = 0; i < (contact_array.length - 1); i++) {
            chai.request(server)
                .post('/contact/')
                .send(contact_array[i])
                .then(function (res) {
                        contact_id_array.push({_id: mongoose.Types.ObjectId(JSON.parse(res.text)._id)});
                    }
                )
        }
        ;
        done();
    };

});


// an array containing the company id of all the test data companies.
var company_id_array = []

// an array containing the contact id of all the test data contacts.
var contact_id_array = []

// Test data which is loaded by insert_company_test_data
var company_array = [
    {title: "Apogee", website: "www.apogee.com", contacts: []},
    {title: "Core Studios", website: "www.corestudios.com", contacts: []},
    {title: "3D Realms", website: "www.3drealms.com", contacts: []},
    {title: "Sierra Studios", website: "www.sierrastudios.com", contacts: []},
    {title: "Naughtydog", website: "www.naughtydog.com", contacts: []},
    {title: "Valve", website: "www.valve.com", contacts: []},
    {title: "Lucasfilm Games", website: "www.lucasfilmgames.com", contacts: []},
    {title: "Square Enix", website: "www.squareenix.com", contacts: []},
    {title: "Sony Computer Entertainment", website: "www.sonycomputerentertainment.com", contacts: []},
    {title: "Konami", website: "www.konami.com", contacts: []},
    {title: "Edios Interactive", website: "www.ediosinteractive.com", contacts: []}
];
